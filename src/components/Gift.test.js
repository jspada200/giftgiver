import React from 'react';
import { shallow } from 'enzyme'
import Gift from './Gift'

describe('Gift', () => {
    const mockRemove = jest.fn();
    const id = 1;
    const props = { gift: { id }, removeGift: mockRemove };
    const gift = shallow(<Gift {...props} />);

    it('renders properly', () => {
        expect(gift).toMatchSnapshot();
    })

    it('init a person and present in state', () => {
        expect(gift.state()).toEqual({
            person: '',
            present: ''
        });
    })

    describe('When typing into person input', () => {
        const simperson = 'Uncle';

        beforeEach(() => {
            gift.find('.input-person').simulate('change', {
                target: { value: simperson }
            })
        })

        it('Update State of Person', () => {
            expect(gift.state().person).toEqual(simperson);
        })

    })


    describe('When typing into present input', () => {
        const sumpresent = 'Box';

        beforeEach(() => {
            gift.find('.input-present').simulate('change', {
                target: { value: sumpresent }
            })
        })

        it('Update State of Present', () => {
            expect(gift.state().present).toEqual(sumpresent);
        })

    })

    describe('When user clicks remove gift', () => {
        beforeEach(() => {
            gift.find('.btn-remove').simulate('click')
        });

        it('it calls the removeGift callback', () => {
            expect(mockRemove).toHaveBeenCalledWith(id);
        });
    });

});

